package com.citiestest.data.error;

import java.io.IOException;

/**
 * Created on 21.12.2017.
 */

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "No connectivity exception";
    }
}
