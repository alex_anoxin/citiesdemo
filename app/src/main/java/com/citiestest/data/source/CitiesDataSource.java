package com.citiestest.data.source;

import com.citiestest.data.City;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 20.12.17.
 */

public interface CitiesDataSource {

    Observable<List<City>> getCities();
}
