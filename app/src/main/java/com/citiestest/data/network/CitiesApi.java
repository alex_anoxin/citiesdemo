package com.citiestest.data.network;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created on 21.12.2017.
 */

public interface CitiesApi {

    String BASE_URL = "https://beta.taxistock.ru/taxik/api/";

    @GET("test/")
    Observable<Response<ResponseBody>> getCities();
}
