package com.citiestest.ui.cities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.citiestest.ui.BaseListAdapter;
import com.citiestest.R;
import com.citiestest.data.City;
import com.citiestest.ui.BaseViewHolder;

import java.util.List;

import butterknife.BindView;

/**
 * Created on 21.12.2017.
 */

public class CitiesAdapter extends BaseListAdapter<City, CitiesAdapter.CityViewHolder> {


    public CitiesAdapter(List<City> items, OnItemClickListener onItemClickListener) {
        super(items, onItemClickListener);
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cities_list_item, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        City city = items.get(position);

        if (city == null) {
            return;
        }
        if (city.getCityName() != null) {
            holder.cityName.setText(city.getCityName());
        }
    }

    static class CityViewHolder extends BaseViewHolder {

        @BindView(R.id.city_name)
        TextView cityName;

        CityViewHolder(View itemView) {
            super(itemView);
        }
    }
}
