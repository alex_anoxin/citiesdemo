package com.citiestest.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

/**
 * Created on 20.12.17.
 */

public abstract class BaseFragment extends DaggerFragment {

    protected final String TAG = BaseFragment.class.getSimpleName();

    protected abstract int getFragmentLayout();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
        setHasOptionsMenu(true);
    }


    /**
     * Setup ButterKnife for View binding.
     *
     * @param view target view
     */
    private void injectViews(final View view) {
        ButterKnife.bind(this, view);
    }

    /**
     * Show Toast message using {@link String} type
     *
     * @param message String message
     */
    protected void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show Toast message using 'int' resource
     *
     * @param resId resource id
     */
    protected void showToast(int resId) {
        Toast.makeText(getContext(), resId, Toast.LENGTH_SHORT).show();
    }
}
