package com.citiestest.ui;

import android.support.v4.app.Fragment;

/**
 * Created on 05.02.2018.
 */

public interface NavigationListener<T extends Fragment> {

    void pushFragment(T fragment);
}
