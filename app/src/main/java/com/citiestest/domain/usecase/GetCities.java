package com.citiestest.domain.usecase;

import android.support.annotation.NonNull;

import com.citiestest.data.City;
import com.citiestest.data.source.CitiesDataSource;
import com.citiestest.domain.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created on 31.01.2018.
 */

public final class GetCities extends UseCase<GetCities.RequestValues, GetCities.ResponseValue> {

    private final CitiesDataSource mCitiesRepository;

    @Inject
    public GetCities(@NonNull CitiesDataSource citiesRepository) {
        mCitiesRepository = checkNotNull(citiesRepository, "citiesRepository cannot be null");
    }

    @Override
    protected Observable<ResponseValue> executeUseCase(RequestValues requestValues) {
        return mCitiesRepository.getCities()
                .flatMap(cities -> Observable.just(new ResponseValue(cities)));
    }

    public static class RequestValues implements UseCase.RequestValues {
    }

    public static class ResponseValue implements UseCase.ResponseValue {

        private final List<City> mCities;

        public ResponseValue(List<City> cities) {
            mCities = checkNotNull(cities, "cities cannot be null!");
        }

        public List<City> getCities() {
            return mCities;
        }
    }
}
