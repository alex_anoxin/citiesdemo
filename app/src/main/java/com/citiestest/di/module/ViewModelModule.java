package com.citiestest.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.citiestest.AppViewModelFactory;
import com.citiestest.di.ViewModelKey;
import com.citiestest.ui.cities.CitiesViewModel;
import com.citiestest.ui.map.MapViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created on 05.02.2018.
 */


@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CitiesViewModel.class)
    abstract ViewModel bindCitiesViewModel(CitiesViewModel citiesViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel.class)
    abstract ViewModel bindMapViewModel(MapViewModel mapViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(AppViewModelFactory factory);
}
